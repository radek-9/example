<?php

namespace App\FrontModule\Presenters;

use App\Entities\Blog;
use App\Entities\BlogTranslation;
use App\Entities\Language;
use Nette\Utils\Strings;

class BlogPresenter extends BasePresenter {

    /** @var \App\Services\Blog @autowire */
    protected $blogService;

    /** @var Blog */
    protected $blog;

    public function startup() {

        parent::startup();

        $this->lang = Language::EN_CODE;

        $this->language = $this->em->getRepository(Language::class)->findOneBy([
            "code" => $this->lang,
        ]);

        $this->template->language = $this->language;
        $this->template->mapLanguage = $this->lang;

        $this->translator->setLocale($this->lang);

        $this->template->translator = $this->translator;

        $this->setLayout('blogLayout');
    }

    /**
     * @param null $filter
     * @param null $search
     * @param null $limit
     */
    public function actionDefault($filter = NULL, $search = NULL, $limit = NULL) {

        $this->redrawControl('snippet');
        $this->redrawControl('snippet2');

        $limit = $limit == 'all' ? NULL : 7; // 1 hlavní + 6
        $this->template->limit = $limit;
        $this->template->filter = $filter;

        $typePairs = [];
        foreach (Blog::getBlogTypePairs() as $key => $blogTypePair) {
            $typePairs[$key] = $this->translator->translate($blogTypePair);
        }

        $filter = $filter == 'all' || !$filter ? $typePairs : [$filter];
        $this->template->search = $search;
        $this->template->blogPosts = $blogPosts = $this->blogService->getBlogPosts($this->translator, $filter, $limit, $search);
        $this->template->mainPost = $mainPosts = $this->blogService->getMainBlogPost($this->translator, $filter);
        $this->template->allBlogPosts = $blogPosts = $this->blogService->getBlogPosts($this->translator, $filter);

        $this->template->getLatte()->addFilter('removeTags', function ($str) {
            $str = Strings::replace($str, array(
                "~\r?\n~" => ' ',
                '~<br[^>]+>\s*~i' => PHP_EOL,
                '~<[^>]*>~' => ' ',
                '~ +~' => ' ',
            ));
            return trim($str);
        });
    }

    /**
     * @param int $id
     * @throws \Nette\Application\BadRequestException
     */
    public function actionDetail($id) {

        $this->blog = $this->em->getRepository(Blog::class)->findOneBy([
            "active" => TRUE,
            "deleted" => FALSE,
            "id" => $id,
        ]);

        if (!$this->blog) {
            $this->error();
        }

        /** @var BlogTranslation $translation */
        $translation = $this->blog->getTranslation($this->language->id);

        if (!$translation) {
            $this->error();
        }

        $this->template->filter = NULL;
        $this->template->blog = $this->blog;
        $this->template->translation = $translation;
    }
}