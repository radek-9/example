<?php

namespace App\Services;

use App\Entities\BlogLink;
use App\Entities\BlogPar;
use App\Entities\BlogPhoto;
use App\Entities\BlogTranslation;
use Nette\Utils\ArrayHash;
use Kdyby\Doctrine\Dql\Join;

class Blog extends Base
{

    /**
     * @param array $filter
     * @param null $limit
     * @param null $search
     * @param null $translator
     * @return mixed
     */
    public function getBlogPosts($translator = NULL, $filter = [], $limit = NULL, $search = NULL) {

        $qb = $this->em->createQueryBuilder()
            ->select("e")
            ->from(\App\Entities\Blog::class, "e")
            ->join("e.translations", "t",Join::WITH, "t.language = :language")
            ->andWhere("e.active = 1")
            ->andWhere("e.deleted = 0")
            ->setParameter("language", \App\Entities\Language::EN)
            ->orderBy("e.created", "DESC");

        $filters = [];

        foreach ($filter as $name) {
            $filters[] = \App\Entities\Blog::getTypeByName($name, $translator);
        }

        if (!empty($filter)) {
            $qb->andWhere("e.type IN (:type)")
                ->setParameter("type", $filters);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        if ($search) {
            $qb->andWhere("t.metaKeywords LIKE :search")
                ->setParameter("search", "%" . $search . "%");
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param null $translator
     * @param array $filter
     * @return \App\Entities\Blog|null
     */
    public function getMainBlogPost($translator = NULL, $filter = []) {

        $mainPost = NULL;

        if ($posts = $this->getBlogPosts($translator, $filter)) {

            /** @var \App\Entities\Blog $mainPost */
            $mainPost = $posts[0];
        }

        return $mainPost;
    }

    /**
     * @param ArrayHash $values
     * @return \App\Entities\Blog|object|null
     * @throws \Exception
     */
    public function saveBlog(ArrayHash $values) {

        if ($values->id) {
            $blog = $this->em->getRepository(\App\Entities\Blog::class)->find($values->id);

        } else {
            $blog = new \App\Entities\Blog();
            $this->em->persist($blog);
        }

        $blog->type = $values->type;
        $blog->author = $values->author;
        $blog->created = $values->created;
        $blog->active = $values->active;

        // linky
        foreach ($blog->links as $link) {
            $this->em->remove($link);
        }
        $blog->links->clear();

        // seřazení podle typu linku
        $valuesLinks = [];
        foreach ($values->links as $blogLink) {
            $valuesLinks[$blogLink->type] = $blogLink;
        }

        ksort($valuesLinks);
        foreach ($valuesLinks as $link) {
            $linkEntity = new BlogLink();
            $linkEntity->type = $link->type;
            $linkEntity->link = $link->link;
            $linkEntity->blog = $blog;

            $this->em->persist($linkEntity);
        }

        $this->em->flush();

        return $blog;
    }

    /**
     * @param ArrayHash $values
     * @param $blogId
     * @return \App\Entities\Blog
     * @throws \Exception
     */
    public function saveBlogPhotos(ArrayHash $values, $blogId) {

        /** @var \App\Entities\Blog $blog */
        $blog = $this->em->getRepository(\App\Entities\Blog::class)->find($blogId);

        if ($values->photo->isOk() && $values->photo->isImage()) {
            $blog->photo = self::storeBlogFile($values->photo, $blog->id);
        }

        // další fotografie
        foreach ($values->photos as $photo) {
            if (!$photo->isOk() || !$photo->isImage()) {
                continue;
            }

            $blogPhoto = new BlogPhoto();
            $blogPhoto->blog = $blog;
            $blogPhoto->path = self::storeBlogFile($photo, $blog->id);

            $this->em->persist($blogPhoto);
        }

        $this->em->flush();

        return $blog;
    }

    /**
     * @param $id
     * @return \Kdyby\Doctrine\EntityManager
     * @throws \Exception
     */
    public function deleteBlog($id) {

        /** @var \App\Entities\Blog $blog */
        $blog = $this->em->getRepository(\App\Entities\Blog::class)->find($id);
        $blog->deleted = TRUE;

        return $this->em->flush($blog);
    }

    /**
     * @param $id
     * @return \App\Entities\Blog
     * @throws \Exception
     */
    public function deleteMainPhoto($id) {

        /** @var \App\Entities\Blog $blog */
        $blog = $this->em->getRepository(\App\Entities\Blog::class)->find($id);

        self::removeFile($blog->getMainPhotoPath());
        $blog->photo = NULL;

        $this->em->flush($blog);

        return $blog;
    }

    /**
     * @param $id
     * @return BlogPhoto
     * @throws \Exception
     */
    public function deletePhoto($id) {

        /** @var BlogPhoto $photo */
        $photo = $this->em->getRepository(BlogPhoto::class)->find($id);

        /** @var BlogPar $blogPars */
        $blogPars = $this->em->getRepository(BlogPar::class)->findAll();

        if ($photo) {

            /** @var BlogPar $par */
            foreach ($blogPars as $par) {
                if ($par->photo && $par->photo->id == $id) {

                    $par->photo = NULL;
                    $this->em->flush($par);
                }
            }

            self::removeFile($photo->path);
            $this->em->remove($photo);
            $this->em->flush($photo);
        }

        return $photo;
    }

    /**
     * @param ArrayHash $values
     * @throws \Doctrine\ORM\ORMException
     */
    public function saveBlogTranslation(ArrayHash $values) {

        if ($values->id) {
            /** @var BlogTranslation $translation */
            $translation = $this->em->getRepository(BlogTranslation::class)->find($values->id);

        } else {
            $translation = new BlogTranslation();
            $translation->blog = $this->em->getReference(\App\Entities\Blog::class, $values->blogId);
            $translation->language = $this->em->getReference(\App\Entities\Language::class, $values->languageId);
        }

        $translation->title = $values->title;
        $translation->photoTitle = $values->photoTitle;
        $translation->metaTitle = $values->metaTitle;
        $translation->metaDescription = $values->metaDescription;
        $translation->metaKeywords = $values->metaKeywords;

        // odstavce
        foreach ($translation->pars as $par) {
            $this->em->remove($par);
        }
        $translation->pars->clear();

        foreach ($values->paragrafs as $par) {

            $parEntity = new BlogPar();
            $parEntity->description = $par->description;
            $parEntity->photo = $par->photo ? $this->em->getReference(BlogPhoto::class, $par->photo) : NULL;
            $parEntity->photoTitle = !empty($par->photoTitle) ? $par->photoTitle : NULL;
            $parEntity->blogTranslation = $translation;

            $this->em->persist($parEntity);
        }

        $this->em->persist($translation);
        $this->em->flush();
    }
}