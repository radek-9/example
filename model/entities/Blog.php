<?php

namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Kdyby\Translation\Translator;

/**
 * @ORM\Entity
 * @ORM\Table(name="blog")
 */
class Blog {

    use TranslationTrait;

    const NEWS = 1;
    const TRAVEL = 2;
    const THINGS_TO_DO = 3;
    const DESTINATIONS = 4;
    const ACCOMMODATION = 5;

    /**
     * @return array
     */
    public static function getBlogTypePairs() {
        return [
            self::NEWS => "app.front.blog.news",
            self::TRAVEL => "app.front.blog.travel",
            self::THINGS_TO_DO => "app.front.blog.thingsToDo",
            self::DESTINATIONS => "app.front.blog.destinations",
            self::ACCOMMODATION => "app.front.blog.accommodation",
        ];
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getTypeName($id) {
        return self::getBlogTypePairs()[$id];
    }

    /**
     * @param $name
     * @param Translator $translator
     * @return mixed
     */
    public static function getTypeByName($name, Translator $translator) {

        $flipped_ = [];

        $flipped = array_flip(self::getBlogTypePairs());

        foreach ($flipped as $key => $val) {
            $flipped_[$translator->translate($key)] = $val;
        }

        return $flipped_[$name];
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    public $id;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", length=11, nullable=false)
     */
    public $type;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=255, nullable=true)
     */
    public $author;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    public $created;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=true)
     */
    public $photo;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="BlogTranslation", mappedBy="blog")
     */
    public $translations;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false, options={"default":0}))
     */
    public $active = FALSE;

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false, options={"default":0}))
     */
    public $deleted = FALSE;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="BlogLink", mappedBy="blog")
     */
    public $links;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="BlogPhoto", mappedBy="blog")
     */
    public $photos;

    public function __construct() {
        $this->translations = new ArrayCollection;
        $this->links = new ArrayCollection;
        $this->photos = new ArrayCollection;
    }

    /**
     * @return string
     */
    public function getMainPhotoPath() {
        return $this->photo;
    }

    /**
     * @return bool
     */
    public function isNews() {
        return $this->type === self::NEWS;
    }

    /**
     * @return bool
     */
    public function isTravel() {
        return $this->type === self::TRAVEL;
    }

    /**
     * @return bool
     */
    public function isThingsToDo() {
        return $this->type === self::THINGS_TO_DO;
    }

    /**
     * @return bool
     */
    public function isDestinations() {
        return $this->type === self::DESTINATIONS;
    }

    /**
     * @return bool
     */
    public function isAccommodation() {
        return $this->type === self::ACCOMMODATION;
    }

    /**
     * @param $id
     * @return string
     */
    public function getTypeColor($id) {

        $color = '#000';

        if ($id === self::NEWS) {
            $color = 'FEBA00';
        }
        elseif ($id === self::TRAVEL) {
            $color = '00F6FF';
        }
        elseif ($id === self::THINGS_TO_DO) {
            $color = '14FC90';
        }
        elseif ($id === self::DESTINATIONS) {
            $color = 'DFF146';
        }
        elseif ($id === self::ACCOMMODATION) {
            $color = 'EE816B';
        }

        return $color;
    }

    /**
     * @return array
     */
    public function getOtherPhotos() {
        $photos = [];

        foreach ($this->photos as $photo) {
            $photos[$photo->id] = $photo->path;
        }

        return $photos;
    }
}