<?php

namespace App\AdminModule\Presenters;


use App\AdminModule\Components\IBlogForm;
use App\AdminModule\Components\IBlogGrid;
use App\AdminModule\Components\IBlogTranslationForm;
use App\Entities\Blog;
use App\Services\Language;

class BlogPresenter extends BaseSecurePresenter {

    /** @var Language @autowire */
    protected $languageService;

    /** @var \App\Services\Blog @autowire */
    protected $blogService;

    public function startup()
    {
        parent::startup();

        if (!$this->user->allowed('blog')) {
            $this->error();
        }
    }

    /**
     * @param null $id
     * @param null $langId
     * @throws \Nette\Application\BadRequestException
     */
    public function actionEdit($id = NULL, $langId = NULL) {

        $langId = (int) $langId;
        $this->template->blog = NULL;
        $this->template->translation = NULL;
        $this->template->langId = $langId;
        $this->template->languages = $this->languageService->getLanguages();

        if ($id) {

            /** @var Blog $blog */
            $blog = $this->em->getRepository(Blog::class)->find($id);

            if (!$blog) {
                $this->error();
            }

            $this->template->blog = $blog;
            $this->template->translation = $blog->getTranslation(\App\Entities\Language::EN);
        }

        if ($id && $langId) {
            $this["blogTranslationForm"]->setBlog($blog, $langId);

            $translation = $blog->getTranslation($langId);

            if ($translation) {
                $this["blogTranslationForm"]->setBlogTranslation($translation);
                $this->template->translation = $translation;
            }

        } elseif($id && !$langId) {
            $this["blogForm"]->setBlog($blog);
        }
    }

    /**
     * @param IBlogGrid $factory
     * @return \App\AdminModule\Components\BlogGrid
     */
    protected function createComponentBlogGrid(IBlogGrid $factory) {
        return $factory->create();
    }

    /**
     * @param IBlogForm $factory
     * @return \App\AdminModule\Components\BlogForm
     */
    protected function createComponentBlogForm(IBlogForm $factory) {
        return $factory->create();
    }

    /**
     * @param IBlogTranslationForm $factory
     * @return \App\AdminModule\Components\BlogTranslationForm
     */
    protected function createComponentBlogTranslationForm(IBlogTranslationForm $factory) {
        return $factory->create();
    }

    /**
     * @param $id
     * @throws \Nette\Application\AbortException
     * @throws \Nette\Application\BadRequestException
     * @throws \Exception
     */
    public function handleDelete($id) {

        if (!$this->user->allowed("blog")) {
            $this->error();
        }

        if ($this->blogService->deleteBlog($id)) {
            $this->flashMessage("Blog smazán", "success");

        } else {
            $this->flashMessage("Blog nelze smazat", "danger");
        }

        $this->redirect("this");
    }

    /**
     * @param $id
     * @throws \Nette\Application\AbortException
     * @throws \Exception
     */
    public function handleDeleteMainPhoto($id) {

        if (!$this->user->allowed("blog")) {
            $this->error();
        }

        if ($this->blogService->deleteMainPhoto($id)) {
            $this->flashMessage("Fotografie smazána", "success");

        } else {
            $this->flashMessage("Fotografii nelze smazat", "danger");
        }

        $this->redirect("this");
    }

    /**
     * @param $photoId
     * @throws \Nette\Application\AbortException
     * @throws \Nette\Application\BadRequestException
     * @throws \Exception
     */
    public function handleDeletePhoto($photoId) {

        if (!$this->user->allowed("blog")) {
            $this->error();
        }

        if ($this->blogService->deletePhoto($photoId)) {
            $this->flashMessage("Fotografie smazána", "success");

        } else {
            $this->flashMessage("Fotografii nelze smazat", "danger");
        }

        $this->redirect("this");
    }

}