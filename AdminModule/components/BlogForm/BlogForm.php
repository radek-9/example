<?php

namespace App\AdminModule\Components;

use App\Entities\Blog;
use App\Entities\BlogLink;
use CnbApi\Utils\DateTime;
use Nette\Application\UI\Form;
use Nette\Forms\Container;
use Vodacek\Forms\Controls\DateInput;

interface IBlogForm {

    /** @return BlogForm */
    function create();
}

class BlogForm extends \App\Components\BaseControl {

    /** @var \App\Services\Blog @autowire */
    protected $blogService;

    /**
     * @param Blog|NULL $blog
     */
    public function setBlog(Blog $blog = NULL) {

        $defaults = [
            "id" => $blog->id ?? NULL,
            "type" => $blog->type ?? NULL,
            "author" => $blog->author ?? NULL,
            "created" => $blog->created ?? NULL,
            "active" => $blog->active ?? NULL,
        ];

        foreach ($blog->links as $link) {
            $defaults["links"][] = [
                "type" => $link->type,
                "link" => $link->link,
            ];
        }

        $this["form"]->setDefaults($defaults);

        $this->template->blog = $blog;
    }

    /**
     * @return Form
     * @throws \Exception
     */
    protected function createComponentForm() {
        $form = new Form;
        $form->setTranslator($this->translator);

        $form->addProtection("Bezpečnosní token vypršel, odešlete formulář znovu.", 10 * 60);

        $form->addHidden("id");

        $form->addSelect("type", "Typ", Blog::getBlogTypePairs())
            ->setPrompt("---")
            ->setRequired("Pole %label je povinné");

        $form->addText("author", "Autor")
            ->setRequired("Pole %label je povinné");

        $form->addDate("created", "Vytvořeno", DateInput::TYPE_DATE)
            ->setAttribute("placeholder", "dd/mm/yyyy")
            ->setDefaultValue(new DateTime())
            ->setRequired("Pole %label je povinné");

        $form->addUpload("photo", "Hlavní fotografie")
            ->setRequired(FALSE)
            ->addRule(Form::IMAGE, "app.front.avatar.image");

        $form->addMultiUpload("photos", "Ostatní fotografie")
            ->setRequired(FALSE)
            ->addRule(Form::IMAGE, "app.front.avatar.image");

        $links = $form->addDynamic('links', function (Container $container) {

            $container->addSelect('type', "Typ", BlogLink::getBlogLinkPairs())
                ->setPrompt("---")
                ->setRequired("Pole %label je povinné");

            $container->addText('link', "Link")
                ->setRequired("Pole %label je povinné");

            $container->addSubmit('remove', 'Remove')
                ->setValidationScope(FALSE)
                ->getControlPrototype()
                ->setName('button')
                ->setHtml("<i class='fa fa-trash'></i>");

            $container["remove"]->onClick[] = [$this, 'removeRow'];
        });

        $links->addSubmit('add', 'Add')
            ->setValidationScope(FALSE)
            ->getControlPrototype()
            ->setName('button')
            ->setHtml("<i class='fa fa-plus'></i>");

        $links["add"]->onClick[] = [$this, 'addRow'];

        $form->addCheckbox("active", "Aktivní");

        $form->addSubmit('send', 'Uložit');

        $form->onSuccess[] = [$this, "formSuccess"];

        return $form;
    }

    /**
     * @param Form $form
     * @throws \Nette\Application\AbortException
     * @throws \Exception
     */
    public function formSuccess(Form $form) {

        if (!$form["send"]->isSubmittedBy()) {
            return;
        }

        $values = $form->getValues();

        /** @var Blog $blog */
        $blog = $this->blogService->saveBlog($values);
        /** @var Blog $blog */
        $blog = $this->blogService->saveBlogPhotos($values, $blog->id);

        $this->presenter->flashMessage("Uloženo", "success");

        if ($values->id) {
            $this->presenter->redirect("this");

        } else {
            $this->presenter->redirect("edit", ["id" => $blog->id]);
        }
    }
}